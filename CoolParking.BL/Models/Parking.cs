﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _instance;

        public Dictionary<string, Vehicle> ListOfVehicle = new();
        public decimal Balance { get; set; } = Settings.InitBalance;

        private Parking()
        {
        }

        public static Parking GetInstance()
        {
            return _instance ??= new Parking();
        }
    }
}
