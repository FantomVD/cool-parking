﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const string IdPattern = @"^([A-Z]{2}-[0-9]{4}-[A-Z]{2})$";

        public static decimal InitBalance { get; } = 0;
        public static int Capacity { get; } = 10;
        public static int PaymentPeriod { get; } = 5000;
        public static int LogWrittingPeriod { get; } = 60000;
        public static decimal PenaltyMultiplier { get; } = 2.5m;
        public static decimal Tariff(VehicleType vehicleType) => vehicleType switch
        {
            VehicleType.PassengerCar => 2,
            VehicleType.Truck => 5,
            VehicleType.Bus => 3.5m,
            VehicleType.Motorcycle => 1
        };


    }
}
