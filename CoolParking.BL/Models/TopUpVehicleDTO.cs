﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class TopUpVehicleDTO
    {
        [JsonProperty("id")]
        public string ID { get; init; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        public TopUpVehicleDTO() { }

    }
}
