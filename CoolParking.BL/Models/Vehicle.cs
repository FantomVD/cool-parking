﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{ 
    public class Vehicle
    {
        // public delegate void BalanceWithdrawnHandler(Vehicle vehicle, decimal balance, decimal sum);
        //
        // public event BalanceWithdrawnHandler BalanceWithdrawn;
        //

        private decimal _balance;
        private string _id;
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; init; }

        [JsonProperty("id")]

        public string Id
        {
            get=>_id;
            init
            {
                // if (!Regex.IsMatch(value, Settings.IdPattern)) throw new ArgumentException("Not valid ID");
                _id = value;
            }
        }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            return $"{(char) random.Next(65, 90)}{(char) random.Next(65, 90)}-" +
                   $"{random.Next(0,9)}{random.Next(0,9)}{random.Next(0,9)}{random.Next(0,9)}-" +
                   $"{(char) random.Next(65, 90)}{(char) random.Next(65, 90)}";
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
            // if (balance < 0) throw new ArgumentException();
        }
        public Vehicle() { Id = "AA-1111-AA"; Balance = 10;
            VehicleType = VehicleType.PassengerCar;
        }
        //
        // public void Take(decimal sum)
        // {
        //     Balance -= sum;
        //     BalanceWithdrawn?.Invoke(this, Balance, sum);
        // }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = vehicleType;
            Balance = balance;
            // if (balance < 0) throw new ArgumentException();
            
        }

        public override string ToString()
        {
            var stringVehicleType = VehicleType switch
            {
                VehicleType.Bus=>"Автобус",
                VehicleType.Motorcycle=>"Мотоцикл",
                VehicleType.Truck=>"Автобус",
                VehicleType.PassengerCar=>"Автомобіль"
            };
            return $"{Id,-15}{stringVehicleType,-15}{Balance,-20}";
        }
    }
}
