﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; init; }
        [JsonProperty("sum")]
        public decimal Sum { get; init; }
        [JsonProperty("transactionDate")]
        public DateTime Time { get; init; }

        public TransactionInfo(string vehicleId, decimal sum, DateTime time)
        {
            this.VehicleId = vehicleId;
            this.Time = time;
            this.Sum = sum;
        }

        public override string ToString()
        {
            return @$"{Time}: {String.Format("{0:0.00}",Sum)} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}
