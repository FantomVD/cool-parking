﻿// TODO: implement enum VehicleType. - Implemented
//       Items: PassengerCar, Truck, Bus, Motorcycle.

using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace  CoolParking.BL.Models
{
    // [DataContract]
    public enum VehicleType:int
    {
        // [EnumMember(Value="Passenger Car")]
        PassengerCar=0,
        // [EnumMember(Value="Track")]

        Truck = 1,
        // [EnumMember(Value="Bus")]

        Bus = 2,

        // [EnumMember(Value="Motorcycle")]
        Motorcycle = 3
    }
}
