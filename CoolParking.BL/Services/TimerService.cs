﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer;
        private double _interval;
        public double Interval
        {
            get=>_interval;
            set
            {
                _interval = value;
                _timer.Interval = Interval;
            }
        }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            _timer = new Timer();
            Interval = interval;
            _timer.Interval = interval;
            _timer.Elapsed += OnTimedEvent;
            Start();
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs args)
        {
            Elapsed?.Invoke(sender,args);
        }


        public void Start()
        {
            _timer.Start();
        }


        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            Stop();
            _timer.Dispose();
        }
    }
}