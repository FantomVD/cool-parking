﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly Parking _parking;
        private List<TransactionInfo> _transactionInfos = new List<TransactionInfo>(); 
        private int FreePlaces { get; set; }

        private void OnTimeWithdraw(object source, ElapsedEventArgs e)
        {
            Withdraw(_parking.ListOfVehicle);
        }
        private  void OnTimeLog(object source, ElapsedEventArgs e)
        {
            
             _logService.Write(string.Join('\n', _transactionInfos.ToList()));
            _transactionInfos.Clear();
        }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _parking = Parking.GetInstance();
            FreePlaces = Settings.Capacity;
            _withdrawTimer.Elapsed += OnTimeWithdraw;
            _logTimer.Elapsed += OnTimeLog;
        }

        private void Withdraw(Dictionary<string, Vehicle> vehicles)
        {
            foreach (var vehicle in vehicles) Withdraw(vehicle.Value);
        }
        private void Withdraw(Vehicle vehicle)
        {
            decimal tariff= Settings.Tariff(vehicle.VehicleType);
            if (vehicle.Balance > tariff)
            {
                vehicle.Balance -= tariff;
                _parking.Balance += tariff;
                _transactionInfos.Add(new TransactionInfo(vehicle.Id,tariff, DateTime.Now));
                
            }
            else if(vehicle.Balance < tariff &&vehicle.Balance>0)
            {
                decimal notEnoughMoney = tariff-vehicle.Balance;
                _parking.Balance += vehicle.Balance;
                _transactionInfos.Add(new TransactionInfo(vehicle.Id, vehicle.Balance, DateTime.Now));
                vehicle.Balance -= vehicle.Balance +notEnoughMoney * Settings.PenaltyMultiplier;
            }
            else
            {
                vehicle.Balance += -tariff * Settings.PenaltyMultiplier;
            }
        }

        public void Dispose()
        {
            _transactionInfos.Clear();
            _parking.ListOfVehicle.Clear();
            _parking.Balance = Settings.InitBalance;
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        public Dictionary<string, Vehicle> GetVehiclesDictionary() => _parking.ListOfVehicle;

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return FreePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.ListOfVehicle.Values.ToList().AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (FreePlaces == 0) throw new InvalidOperationException("No free spaces left");
            if (_parking.ListOfVehicle.ContainsKey(vehicle.Id)) throw new ArgumentException("Vehicle with selected Id doesn't exist");

            _parking.ListOfVehicle.Add(vehicle.Id, vehicle);
            FreePlaces--;

        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!_parking.ListOfVehicle.ContainsKey(vehicleId)) throw new ArgumentException("Vehicle with selected Id doesn't exist");
            if (_parking.ListOfVehicle[vehicleId].Balance < 0) throw new InvalidOperationException("Vehicle with debt can't be removed");
            _parking.ListOfVehicle.Remove(vehicleId);
            FreePlaces++;
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0) throw new ArgumentException("Invalid sum to top up balance");
            if (!_parking.ListOfVehicle.ContainsKey(vehicleId))throw new ArgumentException("Vehicle with selected Id doesn't exist");
            if (_parking.ListOfVehicle[vehicleId].Balance < 0)
            {
                if (Math.Abs(_parking.ListOfVehicle[vehicleId].Balance) <= sum)
                {
                    _parking.Balance +=Math.Abs(_parking.ListOfVehicle[vehicleId].Balance);
                    _transactionInfos.Add(new TransactionInfo(vehicleId,Math.Abs( _parking.ListOfVehicle[vehicleId].Balance), DateTime.Now));
                }
                else
                {
                    _parking.Balance += sum;
                    _transactionInfos.Add(new TransactionInfo(vehicleId, sum, DateTime.Now));

                }
            }
            _parking.ListOfVehicle[vehicleId].Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfos.ToArray();
        }

        public string ReadFromLog()
        {

           return _logService.Read();
        }
    }
}