﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parking;
        public ParkingController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet]
        public async Task<ActionResult<decimal>> Balance()
        {
           return Ok(JsonConvert.SerializeObject(_parking.GetBalance()));
        }

        [HttpGet]
        public async Task<ActionResult<int>> Capacity()
        {
            return Ok(JsonConvert.SerializeObject(_parking.GetCapacity()));
        } 
        [HttpGet]
        public async Task<ActionResult<int>> FreePlaces()
        {
            return Ok(JsonConvert.SerializeObject(_parking.GetFreePlaces()));
        } 

    }
}
