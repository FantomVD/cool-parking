﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;

//TODO Implement methods

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parking;
        public VehiclesController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet]
        public ActionResult<Vehicle[]> Vehicles() => Ok(JsonConvert.SerializeObject(_parking.GetVehicles().ToArray()));

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetById([Required]string id)
        {
            try
            {
                if (!Regex.IsMatch(id, Settings.IdPattern)) throw new ArgumentOutOfRangeException();
                return Ok(JsonConvert.SerializeObject(_parking
                    .GetVehicles()
                    .First(x => x.Id.Equals(id))));

            }
            catch (ArgumentOutOfRangeException exception)
            {
                return BadRequest("Invalid id");

            }
       
            catch (InvalidOperationException exception)
            {
                return NotFound("Vehicle with this ID not found");
            }
        }

        [HttpPost]
        public ActionResult<Vehicle> Vehicles(Vehicle vehicle)
        {
            try
            {
                // Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(json);
                if (!Regex.IsMatch(vehicle.Id, Settings.IdPattern) || vehicle.Balance < 0||!Enum.IsDefined(typeof(VehicleType),vehicle.VehicleType))
                    throw new ArgumentException("Incorrect data");
                _parking.AddVehicle(vehicle);
                return CreatedAtAction(nameof(GetById),nameof(Vehicles),new {id=vehicle.Id},vehicle);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteById([Required] string id)
        {
            try
            {
                if (!Regex.IsMatch(id, Settings.IdPattern)) throw new ArgumentOutOfRangeException("Incorrect ID");
                _parking.RemoveVehicle(id);
                return NoContent();
            }
            catch (InvalidOperationException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (ArgumentOutOfRangeException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (ArgumentException exception)
            {
                return NotFound(exception.Message);
            }
          
        }




    }
}
