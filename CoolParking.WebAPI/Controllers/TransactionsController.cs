﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;


namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parking;
        public TransactionsController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet]
        public ActionResult<TransactionInfo[]> Last()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(_parking.GetLastParkingTransactions()));

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpGet]
        public ActionResult<string> All()
        {
            try
            {
                return Ok(_parking.ReadFromLog());
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<Vehicle> TopUpVehicle(TopUpVehicleDTO info)
        {
            try
            {
                if (!Regex.IsMatch(info.ID, Settings.IdPattern) || info.Sum <= 0)
                    throw new ArgumentOutOfRangeException("Invalid arguments");
                _parking.TopUpVehicle(info.ID, info.Sum);
                return Ok(_parking.GetVehicles().First(x => x.Id == info.ID));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        } 
    }
}
