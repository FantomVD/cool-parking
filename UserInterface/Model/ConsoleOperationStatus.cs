﻿namespace UserInterface.Model
{
    public enum ConsoleOperationStatus
    {
        Default,
        Successful,
        Error,
        Heading
    }
}