﻿using System;
using System.Text;
using CoolParking.BL.Models;

namespace UserInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding=Encoding.UTF8;
            (new ConsoleInterface()).Run();

        }
    }
}