﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using UserInterface.Model;

namespace UserInterface
{
    public partial class ConsoleInterface
    {
        private HttpClient _client = new();
        public ConsoleInterface()
        {
            Console.OutputEncoding = Encoding.Unicode;
        }

        public void Run()
        {
            int vubir = 1;
            while(vubir != 0)
            {
                try
                {
                    ChangeConsoleColor(ConsoleOperationStatus.Heading);
                    Console.OutputEncoding = Encoding.UTF8;
                    Console.WriteLine(
                        "Вiтаємо в програмi Cool Parking! \nНижче наданий перелiк дiй, якi ви можете виконувати\n" +
                        "Вводьте цифри для вiдповiдних дiй:");
                    ChangeConsoleColor(ConsoleOperationStatus.Default);
                    Console.WriteLine(
                        "1 - Вивести на екран поточний баланс Паркiнгу\n" +
                        "2 - Вивести на екран суму зароблених коштiв за поточний перiод\n" +
                        "3 - Вивести на екран кiлькiсть вiльних мiсць на паркуваннi\n" +
                        "4 - Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод\n" +
                        "5 - Вивести на екран iсторiю Транзакцiй\n" +
                        "6 - Вивести на екран список Транспорту, що знаходяться на Паркiнгу\n" +
                        "7 - Поставити Транспортний засiб на Паркiнг\n" +
                        "8 - Забрати Транспортний засiб з Паркiнгу\n" +
                        "9 - Поповнити баланс конкретного Тр. засобу\n" +
                        "0 - Вихiд з програми\n");
                    vubir = Convert.ToInt32(Console.ReadLine());
                   Console.WriteLine(CallFunctionByNumber(vubir));
                   Console.WriteLine("\nНатисніть любу кнопку, щоб продовжити");
                    Console.ReadKey();
                    Console.Clear();
                }
                catch (FormatException ex)
                {
                    ChangeConsoleColor(ConsoleOperationStatus.Error);
                    Console.WriteLine("Введенi не коректнi данi");
                    Console.WriteLine(ex.Message);
                    Thread.Sleep(1000);
                    Console.Clear();
                }
                catch (FileNotFoundException ex)
                {
                    Console.WriteLine("Файла не знайдено");
                    Console.WriteLine(ex.Message);

                }

            }

        }
        
        
        private string CallFunctionByNumber(int vubir) => vubir switch
        {
            
            1 => $"Поточний баланс становить: { GetCurrentBalance()}грн.",
            2 => $"Сума зароблених коштiв за поточний перiод становить: {GetTransactionSum()}грн.",
            3 => $"На даний момент вiльно {GetFreePlaces()} з {GetCapacity()}",
          //I think this is bad
            4 => $"{((GetTransactionForThisPeriod().Length!=0)? $"За даний перiод були здiйсненi такi транзакцiї:\n{FormatTransactionForThisPeriod(GetTransactionForThisPeriod())}":$"За даний перiод не було здiйснено транзакцiй")}",
            5 => $"{(string.IsNullOrEmpty(GetTransactionHistory())? "Iсторiя транзакцiй порожня":$"Iсторiя транзакцiй:\n ID транспорту\tЧас\t\tЦiна\n{GetTransactionHistory()}")}",
            6 => $"{((GetVehicles().Length==0)? $"На разi немає машин на Паркiнгу\n": $"На даний момент на паркiнгу перебувають такi машини:\n{FormatOutputTransports(GetVehicles())}")}",
            7 =>
                $"{(InputVehicleData() ? "Транспорт успiшно додано\n":"Виникла помилка\n")}",
            8 => $"{(RemoveVehicle()?"Транспорт успiшно видалено\n":"Виникла помилка")}",
            9 => $"{(TopUp()?"Аккаунт успiшно поповнено\n":"Виникла помилка\n")}",
            0 => $"Триває вихiд",
            _ => "Такої дiї не було в перелiку"
        };


        private decimal GetCurrentBalance()
        {
            return  _client.GetFromJsonAsync<decimal>("https://localhost:44356/api/Parking/Balance").Result;
            
        }

        private decimal GetTransactionSum()
        {
            return _client.GetFromJsonAsync<TransactionInfo[]>("https://localhost:44356/api/Transactions/Last")
                .Result
                .Sum(x=>x.Sum);
        }
        private int GetFreePlaces()=>_client.GetFromJsonAsync<int>("https://localhost:44356/api/Parking/FreePlaces").Result;
        
        private int GetCapacity() => _client.GetFromJsonAsync<int>("https://localhost:44356/api/Parking/Capacity").Result;
        
        private string FormatTransactionForThisPeriod(TransactionInfo[] transactions) => string.Join("\n",transactions);

        private TransactionInfo[] GetTransactionForThisPeriod()
        {
            var response = _client.GetAsync("https://localhost:44356/api/Transactions/Last").Result;
            return JsonConvert.DeserializeObject<TransactionInfo[]>(response.Content.ReadAsStringAsync().Result);
        }

        private string GetTransactionHistory()
        {
            var response = _client.GetAsync("https://localhost:44356/api/Transactions/All").Result;
            if (response.StatusCode == HttpStatusCode.NotFound) return "";
            return response.Content.ReadAsStringAsync().Result;
        }

        private Vehicle[] GetVehicles() =>
            _client.GetFromJsonAsync<Vehicle[]>("https://localhost:44356/api/Vehicles").Result;
        private string FormatOutputTransports(Vehicle[] vehicles) => string.Join("\n", vehicles.Select(x=>x.ToString()));

        private bool InputVehicleData()
        {
            try
            {
                var id =InputTransportID();
                var vehicleType = InputVehicleType();
                var balance = InputBalance();
                AddVehicle(id,vehicleType,balance);
                ChangeConsoleColor(ConsoleOperationStatus.Successful);
                return true;
            }
            catch (ArgumentException e)
            {
                ChangeConsoleColor(ConsoleOperationStatus.Error);
                Console.WriteLine(e.Message);
            }
            catch (FormatException e)
            {
                ChangeConsoleColor(ConsoleOperationStatus.Error);
                Console.WriteLine("Не вдалось конвертувати стрiчку в число");
            }

            return false;
        }

        private void AddVehicle(string id, VehicleType vehicleType, decimal balance)
        {
            var jsonContent = JsonConvert.SerializeObject(new Vehicle(id, vehicleType, balance)).ToString();

            var response = _client.PostAsync("https://localhost:44356/api/Vehicles",
                new StringContent(jsonContent, Encoding.UTF8, "application/json")).Result;
            if (response.StatusCode == HttpStatusCode.BadRequest) throw new ArgumentException("Некоректна вхідна інформація");

        }

        private bool RemoveVehicle()
        {

            try
            {
                string id = InputTransportID();
                if (String.IsNullOrEmpty(id)) throw new ArgumentException("Необхідно ввести ID");
                var response = _client.DeleteAsync($"https://localhost:44356/api/Vehicles/{id}").Result;
                if (response.StatusCode == HttpStatusCode.BadRequest) throw new ArgumentException("Некоректний ID");
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new ArgumentException("Машину з даним ID не знайдено");
                ChangeConsoleColor(ConsoleOperationStatus.Successful);
                return true;
            }
            catch (ArgumentException e)
            {
                ChangeConsoleColor(ConsoleOperationStatus.Error);
                Console.WriteLine(e.Message);
                return false;
            }
            
        }

        private string InputTransportID()
        {
            Console.WriteLine("Номер машини у форматi XX-YYYY-XX, де Х - лiтера латинського алфавiту у верхньому регiстрi, Y - будь-яка цифра");
                string id = Console.ReadLine();
                //if (!Regex.IsMatch(id, Settings.IdPattern)) throw new ArgumentException("Некоректна форма ID");
                return id;
        }

        private VehicleType InputVehicleType()
        {
            Console.WriteLine("Виберiть тип машини:\n0 - Легкова машина\n1 - Вантажна\n2 - Автобус\n3 - Мотоцикл");
            VehicleType vehicleType = Convert.ToInt32(Console.ReadLine()) switch
            {
                0 => VehicleType.PassengerCar,
                1 => VehicleType.Truck,
                2 => VehicleType.Bus,
                3 => VehicleType.Motorcycle,
                _ => throw new ArgumentException()
            };
            return vehicleType;
        }

        private decimal InputBalance()
        {
            Console.WriteLine("Вкажiть суму");
            decimal balance = Convert.ToDecimal(Console.ReadLine());
            // if (balance < 0) throw new ArgumentException("Баланс не можна створювати від'ємним");
            return balance;
        }
        private bool TopUp()
        {
            
            try
            {
                var id = InputTransportID();
                var sum = InputBalance();
                var json = JsonConvert.SerializeObject(new TopUpVehicleDTO {ID = id, Sum = sum});
                var response = _client.PutAsync("https://localhost:44356/api/Transactions/TopUpVehicle",
                    new StringContent(json, Encoding.UTF8, "application/json")).Result;
                if (response.StatusCode == HttpStatusCode.BadRequest)
                    throw new ArgumentException("Некоректні вхідні дані");
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new ArgumentException("Машину із заданим ID не знайдено");
                ChangeConsoleColor(ConsoleOperationStatus.Successful);
                return true;
            }
            catch (ArgumentException e)
            {
                ChangeConsoleColor(ConsoleOperationStatus.Error);
                Console.WriteLine("Некоректнi данi");
                return false;
            }
            catch (FormatException e)
            {
                ChangeConsoleColor(ConsoleOperationStatus.Error);
                Console.WriteLine("Не вдалось конвертувати");
                return false;
            }
        }

        private void ChangeConsoleColor(ConsoleOperationStatus operationStatus)=>
            Console.ForegroundColor = operationStatus switch
            {
                ConsoleOperationStatus.Default =>  ConsoleColor.White,
                ConsoleOperationStatus.Successful =>  ConsoleColor.Green,
                ConsoleOperationStatus.Error => ConsoleColor.Red,
                ConsoleOperationStatus.Heading => ConsoleColor.Cyan
            };
        
    }
}